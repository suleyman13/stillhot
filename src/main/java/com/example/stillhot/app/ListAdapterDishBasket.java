package com.example.stillhot.app;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.example.stillhot.app.back.DatabaseHelper;
import com.example.stillhot.app.back.Formatting;
import com.example.stillhot.app.back.maps.Dish;
import com.example.stillhot.app.ui.MyTextViewBold;
import com.example.stillhot.app.ui.MyTextViewLight;

import java.util.ArrayList;

/**
 * Created by Admin on 11.10.2015.
 */
public class ListAdapterDishBasket extends ArrayAdapter<Dish> {
    int res;
    ArrayList<Dish> dishes;
    RelativeLayout totalLayout, minSumLayout, rl_min_sum_order;
    int priceMin;
    RelativeLayout rlOrederLeft;
    MyTextViewLight textViewLight;

    public ListAdapterDishBasket(Context context, int resource, ArrayList<Dish> restaurants, RelativeLayout totalLayout, RelativeLayout minSumLayout, int priceMin, RelativeLayout rlOrederLeft, MyTextViewLight textViewLight) {
        super(context, resource, restaurants);
        this.res = resource;
        this.dishes = restaurants;
        this.totalLayout = totalLayout;
        this.minSumLayout = minSumLayout;
        this.rl_min_sum_order = (RelativeLayout) minSumLayout.findViewById(R.id.rl_min_sum_order);
        this.priceMin = priceMin;
        this.rlOrederLeft = rlOrederLeft;
        this.textViewLight = textViewLight;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder viewHolder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(res, parent, false);// View.inflate(getContext(), res, null); //inflater.inflate(R.layout.list_item, viewGroup, false);
            viewHolder = new ViewHolder();
            viewHolder.name = (MyTextViewLight) convertView.findViewById(R.id.textView46);
            viewHolder.desc = (MyTextViewLight) convertView.findViewById(R.id.textView47);
            viewHolder.price = (MyTextViewLight) convertView.findViewById(R.id.textView48);
            viewHolder.tv_count = (MyTextViewBold) convertView.findViewById(R.id.textView49);
            viewHolder.tv_minus = (TextView) convertView.findViewById(R.id.textView50);
            viewHolder.tv_plus = (TextView) convertView.findViewById(R.id.textView51);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.name.setText(dishes.get(position).getTitle());
        viewHolder.desc.setText(dishes.get(position).getDescr());
        viewHolder.price.setText("" + dishes.get(position).getPrice());
        int count = DatabaseHelper.getDishCount(getContext(), dishes.get(position).getId());
        if (count != 0) {
            viewHolder.tv_count.setVisibility(View.VISIBLE);
            viewHolder.tv_count.setText("" + count);
        } else {
            viewHolder.tv_count.setVisibility(View.INVISIBLE);
            viewHolder.tv_count.setText("0");
        }
        viewHolder.tv_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count = DatabaseHelper.deductDishCount(getContext(), dishes.get(position).getId());
                if (count <= 0) {
                    checkSum();
                    viewHolder.tv_minus.setEnabled(false);
                    viewHolder.tv_count.setText("");
                    viewHolder.tv_count.setVisibility(View.INVISIBLE);
                } else {
                    viewHolder.tv_count.setText("" + count);
                    viewHolder.tv_count.setVisibility(View.VISIBLE);
                }
                checkSum();
            }
        });

        viewHolder.tv_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewHolder.tv_count.setText("" + DatabaseHelper.addDishCount(getContext(), dishes.get(position).getId()));
                viewHolder.tv_count.setVisibility(View.VISIBLE);
                viewHolder.tv_minus.setEnabled(true);
                checkSum();
            }
        });
        return convertView;
    }

    /*private synchronized void checkBasket() {
        int count = DatabaseHelper.checkBasketCount(getContext());
        TextView tv_basketCount = (TextView) basketFloor.findViewById(R.id.textView15);
        tv_basketCount.setText("" + count);
        basketFloor.setVisibility((count <= 0) ? View.GONE : View.VISIBLE);
    }*/

    private synchronized void checkSum() {
        int sumBasket = DatabaseHelper.getSumBasket(getContext());
        TextView sum = (TextView) totalLayout.findViewById(R.id.textView40);
        TextView total = (TextView) totalLayout.findViewById(R.id.textView41);
        sum.setText("" + sumBasket);
        total.setText("" + sumBasket);

        int howToNeed = sumBasket - priceMin;
        if (howToNeed < 0) {
            rl_min_sum_order.setVisibility(View.VISIBLE);
            TextView minSum = (TextView) rl_min_sum_order.findViewById(R.id.textView35);
            minSum.setText("" + Formatting.formatCount(new String[]{"рубль", "рубля", "рублей"}, (long) (-howToNeed)));
            rlOrederLeft.setVisibility(View.GONE);
            textViewLight.setVisibility(View.GONE);
        } else {
            rl_min_sum_order.setVisibility(View.GONE);
            TextView tv_bonus = (TextView) rlOrederLeft.findViewById(R.id.textView37_);
            tv_bonus.setText("+ " + Formatting.formatCount(new String[]{"бонус", "бонуса", "бонусов"}, (long) (sumBasket * 0.1)) + "!");
            rlOrederLeft.setVisibility(View.VISIBLE);
            textViewLight.setVisibility(View.VISIBLE);
        }
    }

    class ViewHolder {
        MyTextViewLight name, desc, price;
        MyTextViewBold tv_count;
        TextView tv_minus, tv_plus;
        //ImageView image;
    }
}
