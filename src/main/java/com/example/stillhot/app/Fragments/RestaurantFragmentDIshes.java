package com.example.stillhot.app.Fragments;

import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.example.stillhot.app.*;
import com.example.stillhot.app.MainActivity;
import com.example.stillhot.app.back.Imageloader;
import com.example.stillhot.app.back.Preferences;
import com.example.stillhot.app.back.maps.Restaurant;
import com.example.stillhot.app.pojo.*;
import com.google.gson.Gson;
import com.nineoldandroids.view.ViewHelper;
import com.nineoldandroids.view.ViewPropertyAnimator;

/**
 * Created by Admin on 16.10.2015.
 */
public class RestaurantFragmentDishes extends BaseMainFragment implements ObservableScrollViewCallbacks {
    private int headerTranslation = 0;
    private View mHeaderView;
    private View mToolbarView;
    private int mBaseTranslationY;
    private ViewPager mPager;
    private NavigationAdapter mPagerAdapter;
    private int prev = 0;
    SlidingTabLayout slidingTabLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = (ViewGroup) inflater.inflate(R.layout.activity_main, null);
        final Gson gson = new Gson();
        MainActivity.currentFragment = "com.example.stillhot.app.Fragments.RestaurantFragmentDishes";
        Restaurant restaurant = gson.fromJson(Preferences.get().getString(Preferences.RESTAURANT), Restaurant.class);
        com.example.stillhot.app.MainActivity.setVisibilityForLogo();
        TextView tv_rest_name = (TextView) root.findViewById(R.id.textView3);
        tv_rest_name.setText(restaurant.getTitle());
        TextView tv_rest_cooking = (TextView) root.findViewById(R.id.textView4);
        tv_rest_cooking.setText(restaurant.getDesc());
        ImageView iv_logo = (ImageView) root.findViewById(R.id.imageView);
        Imageloader.get().loadImage(restaurant.getLogo(), iv_logo);
        /*if (!TextUtils.isEmpty(restaurant.getLogo()))
            Picasso.with(getActivity()).load(restaurant.getLogo()).into(iv_logo);*/
        TextView tv_min_sum_zak = (TextView) root.findViewById(R.id.textView5);
        tv_min_sum_zak.setText("" + restaurant.getPrice_min());
        TextView tv_min_del_time = (TextView) root.findViewById(R.id.textView26);
        tv_min_del_time.setText("" + restaurant.getDelivery_time() + " мин.");
        TextView tv_del_type = (TextView) root.findViewById(R.id.textView24);
        tv_del_type.setText(restaurant.isDelivery_free() ? "Бесплатно" : "Платно");
        TextView tv_min_del_ratio = (TextView) root.findViewById(R.id.textView30);
        tv_min_del_ratio.setText(restaurant.getRatio() + "%");

        mHeaderView = root.findViewById(R.id.header);
        ViewCompat.setElevation(mHeaderView, getResources().getDimension(R.dimen.toolbar_elevation));
        mToolbarView = root.findViewById(R.id.rl_rest_header);
        mPagerAdapter = newViewPagerAdapter();
        mPager = (ViewPager) root.findViewById(R.id.pager);
        mPager.setAdapter(mPagerAdapter);
        mPager.setOffscreenPageLimit(3);

        slidingTabLayout = (SlidingTabLayout) root.findViewById(R.id.sliding_tabs);
        slidingTabLayout.setCustomTabView(R.layout.tab_indicator, android.R.id.text1);
        slidingTabLayout.setSelectedIndicatorColors(getResources().getColor(R.color.accent));
        slidingTabLayout.setDistributeEvenly(true);
        slidingTabLayout.setViewPager(mPager);

        boolean isFeedBack = false;
        Bundle args = getArguments();
        if (args != null)
            isFeedBack = args.getBoolean("feedBack", false);
        if (isFeedBack) {
            mPager.setCurrentItem(2);
            SlidingTabStrip strip = (SlidingTabStrip) slidingTabLayout.getChildAt(0);
            TextView prevTitle = (TextView) strip.getChildAt(prev);
            prevTitle.setTextColor(Color.rgb(255, 255, 255));
            TextView title = (TextView) strip.getChildAt(2);
            title.setTextColor(Color.rgb(0, 0, 0));
            prev = 2;
            /*propagateToolbarState(toolbarIsShown()); sull */
        }


        // When the page is selected, other fragments' scrollY should be adjusted
        // according to the toolbar status(shown/hidden)
        slidingTabLayout.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {

            }

            @Override
            public void onPageSelected(int i) {
                SlidingTabStrip strip = (SlidingTabStrip) slidingTabLayout.getChildAt(0);
                TextView prevTitle = (TextView) strip.getChildAt(prev);
                prevTitle.setTextColor(Color.rgb(255, 255, 255));
                TextView title = (TextView) strip.getChildAt(i);
                title.setTextColor(Color.rgb(0, 0, 0));
                prev = i;
                //propagateToolbarState(toolbarIsShown());
                changeScrolTop(toolbarIsShown(), i);
                //onScrollChanged(0, false, false);
                //onUpOrCancelMotionEvent(ScrollState.STOP);

//                Fragment f = mPagerAdapter.getItemAt(i);
//                View view = f.getView();
//
//                ObservableScrollView scrollView = (ObservableScrollView) view.findViewById(R.id.scroll);
//                View innerView = (LinearLayout) view.findViewById(R.id.root_layout);
//                int height = innerView.getHeight();
//                int[] location = new int[2];
//                innerView.getLocationOnScreen(location);
//                int h = -headerTranslation;
//                if (location[1] + getStatusBarHeight() < h)
//                    scrollView.scrollTo(0, 0);
//                else
//                    scrollView.scrollTo(0, location[1] + getStatusBarHeight());

            }

            @Override
            public void onPageScrollStateChanged(int i) {
            }
        });

        propagateToolbarState(toolbarIsShown());
        return root;
    }

    public void changeScrolTop(boolean isShown,  int i) {
        float deltaHeight = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics());
        int toolbarHeight = mToolbarView.getHeight();
        Fragment f = mPagerAdapter.getItemAt(i);
        int pTabNorm = toolbarHeight
                + getStatusBarHeight()
                /*+ getToolbarHeight()*/
                + slidingTabLayout.getHeight()
                + (int)deltaHeight;

        int pVNorm = getStatusBarHeight()
                + getToolbarHeight()
                + slidingTabLayout.getHeight()
                + (int)deltaHeight;

        // Skip destroyed or not created item
        if (f == null) {
            return;
        }
        View view = f.getView();
        if (view == null) {
            return;
        }
        ObservableScrollView scrollView = (ObservableScrollView) view.findViewById(R.id.scroll);
        ViewGroup innerView = (LinearLayout) view.findViewById(R.id.root_layout);
        int height = innerView.getHeight();
        int[] locInScreen = new int[2];
        slidingTabLayout.getLocationOnScreen(locInScreen);
        int[] location = new int[2];
        innerView.getLocationOnScreen(location);
        //int h = -headerTranslation;

        int pTabNew = locInScreen[1];
        int pVC = location[1];
        int deltapTab = pTabNorm - pTabNew;
        int deltapV = pVNorm - pVC;

        if (deltapV > deltapTab) {
            scrollView.scrollTo(0, deltapTab);
        } else {
            int hV = height - toolbarHeight - pVNorm;
            int hP = hV - getStatusBarHeight() - getToolbarHeight() - slidingTabLayout.getHeight();
            scrollView.scrollTo(0, Math.min(hV, deltapTab));
            //onScrollChanged(-Math.min(hV, deltapTab), false, false);
            ViewPropertyAnimator.animate(mHeaderView).cancel();
            ViewHelper.setTranslationY(mHeaderView, -Math.min(hP > 0 ? hP : 0, deltapTab));
        }

        /*if (isShown) {
            // Scroll up
            if (0 < scrollView.getCurrentScrollY()) {
                //slidingTabLayout.scrollTo(0, location[1] + getStatusBarHeight());
                int cTop = slidingTabLayout.getTop();
                scrollView.scrollTo(0, headerTranslation);
            }
        } else {
            // Scroll down (to hide padding)
            if (scrollView.getCurrentScrollY() < toolbarHeight) {
                //slidingTabLayout.scrollTo(0, location[1] + getStatusBarHeight());
                int cTop = slidingTabLayout.getTop();
                int[] locInScreen = new int[2];
                slidingTabLayout.getLocationOnScreen(locInScreen);
                int[] licInWindow = new int[2];
                slidingTabLayout.getLocationInWindow(licInWindow);
                int he = slidingTabLayout.getHeight();
                scrollView.scrollTo(0, headerTranslation);
                //scrollView.scrollTo(0, location[1] + getStatusBarHeight());
            }
        }*/
    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
//        if (dragging) {
        int toolbarHeight = mToolbarView.getHeight();
        float currentHeaderTranslationY = ViewHelper.getTranslationY(mHeaderView);
        if (firstScroll) {
            if (-toolbarHeight < currentHeaderTranslationY) {
                //               mBaseTranslationY = scrollY;
            }
        }

        final float headerTranslationY = ScrollUtils.getFloat(-(scrollY - mBaseTranslationY), -toolbarHeight, 0);
        headerTranslation = (int) headerTranslationY;
        ViewPropertyAnimator.animate(mHeaderView).cancel();
        ViewHelper.setTranslationY(mHeaderView, headerTranslationY);
//            mHeaderView.setY(headerTranslationY);
    }

    @Override
    public void onDownMotionEvent() {
    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {
        mBaseTranslationY = 0;

        Fragment fragment = getCurrentFragment();
        if (fragment == null) {
            return;
        }
        View view = fragment.getView();
        if (view == null) {
            return;
        }

        int toolbarHeight = mToolbarView.getHeight();
        final ObservableScrollView scrollView = (ObservableScrollView) view.findViewById(R.id.scroll);
        if (scrollView == null) {
            return;
        }
        int scrollY = scrollView.getCurrentScrollY();
        if (scrollState == ScrollState.DOWN) {
            //          showToolbar();
        } else if (scrollState == ScrollState.UP) {
            if (toolbarHeight <= scrollY) {
//                hideToolbar();
            } else {
//                showToolbar();
            }
        } else {
            // Even if onScrollChanged occurs without scrollY changing, toolbar should be adjusted
            if (toolbarIsShown() || toolbarIsHidden()) {
                // Toolbar is completely moved, so just keep its state
                // and propagate it to other pages
                /*propagateToolbarState(toolbarIsShown()); sull*/
            } else {
                // Toolbar is moving but doesn't know which to move:
                // you can change this to hideToolbar()
//                showToolbar();
            }
        }
    }

    private Fragment getCurrentFragment() {
        return mPagerAdapter.getItemAt(mPager.getCurrentItem());
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public int getToolbarHeight() {
        Resources resources = getActivity().getResources();
        int resourceId = getResources().getIdentifier("navigation_bar_height", "dimen", "android");
        if (resourceId > 0) {
            int i = resources.getDimensionPixelSize(resourceId);
            return i;
        }
        return 0;
    }

    private void propagateToolbarState(boolean isShown) {
        int toolbarHeight = mToolbarView.getHeight();

        // Set scrollY for the fragments that are not created yet
        mPagerAdapter.setScrollY(-headerTranslation);



        // Set scrollY for the active fragments
        for (int i = 0; i < mPagerAdapter.getCount(); i++) {

            // Skip destroyed or not created item
            Fragment f = mPagerAdapter.getItemAt(i);
            if (f == null) {
                continue;
            }

            View view = f.getView();
            if (view == null) {
                continue;
            }
            ObservableScrollView scrollView = (ObservableScrollView) view.findViewById(R.id.scroll);
            /*View innerView = (LinearLayout) view.findViewById(R.id.root_layout);
            int height = innerView.getHeight();
            int[] location = new int[2];
            innerView.getLocationOnScreen(location);
            int h = -headerTranslation;
            scrollView.scrollTo(0, -headerTranslation);*/
            View innerView = (LinearLayout) view.findViewById(R.id.root_layout);
            int height = innerView.getHeight();
            int[] location = new int[2];
            innerView.getLocationOnScreen(location);
            int h = -headerTranslation;
//            if (location[1] + getStatusBarHeight() < h)
//                scrollView.scrollTo(0, 0);
//            else
//                scrollView.scrollTo(0, location[1] + getStatusBarHeight());

//             Skip current item
            if (i == mPager.getCurrentItem()) {
                continue;
            }
            if (isShown) {
                // Scroll up
                if (0 < scrollView.getCurrentScrollY()) {
                    //slidingTabLayout.scrollTo(0, location[1] + getStatusBarHeight());
                    scrollView.scrollTo(0, headerTranslation);
                }
            } else {
                // Scroll down (to hide padding)
                if (scrollView.getCurrentScrollY() < toolbarHeight) {
                    //slidingTabLayout.scrollTo(0, location[1] + getStatusBarHeight());
                    scrollView.scrollTo(0, headerTranslation);
                    //scrollView.scrollTo(0, location[1] + getStatusBarHeight());
                }
            }
        }
    }

    protected NavigationAdapter newViewPagerAdapter() {
        return new NavigationAdapter(getChildFragmentManager());
    }

    private boolean toolbarIsShown() {
        return ViewHelper.getTranslationY(mHeaderView) == 0;
    }

    private boolean toolbarIsHidden() {
        return ViewHelper.getTranslationY(mHeaderView) == -mToolbarView.getHeight();
    }

    private void showToolbar() {
        float headerTranslationY = ViewHelper.getTranslationY(mHeaderView);
        if (headerTranslationY != 0) {
            ViewPropertyAnimator.animate(mHeaderView).cancel();
            ViewPropertyAnimator.animate(mHeaderView).translationY(0).setDuration(2000).start();
        }
        propagateToolbarState(true);
    }

    private void hideToolbar() {
        float headerTranslationY = ViewHelper.getTranslationY(mHeaderView);
        int toolbarHeight = mToolbarView.getHeight();
        if (headerTranslationY != -toolbarHeight) {
            ViewPropertyAnimator.animate(mHeaderView).cancel();
            ViewPropertyAnimator.animate(mHeaderView).translationY(-toolbarHeight).setDuration(2000).start();
        }
        propagateToolbarState(false);
    }

    protected class NavigationAdapter extends CacheFragmentStatePagerAdapter {

        private final String[] TITLES = new String[]{"МЕНЮ", "ИНФО", "ОТЗЫВЫ"};

        private int mScrollY;

        public NavigationAdapter(FragmentManager fm) {
            super(fm);
        }

        public void setScrollY(int scrollY) {
            mScrollY = scrollY;
        }

        protected Fragment newFragment() {
            return new ViewPagerTabScrollViewFragment();
        }

        @Override
        protected Fragment createItem(int position) {
            Fragment f;
            if (position == 0) {
                f = new MenuFragment();
            } else if (position == 1) {
                f = new RestInfoFragment();
            } else {
                f = new RestFeedBack();
            }
            if (0 <= mScrollY) {
                Bundle args = new Bundle();
                args.putInt(ViewPagerTabScrollViewFragment.ARG_SCROLL_Y, mScrollY);
                f.setArguments(args);
            }
            return f;
        }

        @Override
        public int getCount() {
            return TITLES.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }
    }
}