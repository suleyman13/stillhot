package com.example.stillhot.app.Fragments;

import android.app.Service;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.stillhot.app.ListAdapterRestaurant;
import com.example.stillhot.app.MainActivity;
import com.example.stillhot.app.R;
import com.example.stillhot.app.back.*;
import com.example.stillhot.app.back.maps.*;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * Created by dev on 14.10.2015.
 */
public class RestaurantFragment extends Fragment {
    ListAdapterRestaurant adapter;
    ListView list;
    ImageView basket;
    TextView count;
    ProgressBar progressBar;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final ViewGroup root = (ViewGroup) inflater.inflate(R.layout.activity_four_items_list, null);
        MainActivity.currentFragment = "com.example.stillhot.app.Fragments.RestaurantFragment";
//        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//        imm.hideSoftInputFromWindow(root.getWindowToken(), 0);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        progressBar = (ProgressBar) root.findViewById(R.id.progressBar);
        list = (ListView) root.findViewById(R.id.listView);
        ;
        //MainActivity.filter_top.clearFocus();
        final ArrayList<Restaurant> restaurants = new ArrayList<>();
        final ArrayList<Restaurant> restaurants_temp = new ArrayList<>();
        adapter = new ListAdapterRestaurant(getActivity(), R.layout.restaurants_item_temp, restaurants);
        basket = (ImageView) root.findViewById(R.id.imageView11);
        count = (TextView) root.findViewById(R.id.textView31);
        int countDish = DatabaseHelper.checkBasketCount(getActivity());
        setBasketVisibility(countDish);
        basket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.fragmentManager.beginTransaction()
                        .replace(R.id.container, Fragment.instantiate(getActivity(), "com.example.stillhot.app.Fragments.BasketFragment"))
                        .commit();
            }
        });
        count.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.fragmentManager.beginTransaction()
                        .replace(R.id.container, Fragment.instantiate(getActivity(), "com.example.stillhot.app.Fragments.BasketFragment"))
                        .commit();
            }
        });

        RelativeLayout basketFloor = (RelativeLayout) root.findViewById(R.id.rl_bottom);
        basketFloor.setVisibility(View.VISIBLE);
        /*if (countDish <= 0) {
            basketFloor.setVisibility(View.GONE);
            list.setLayoutParams(new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT,
                    RelativeLayout.LayoutParams.MATCH_PARENT));
        } else {
            basketFloor.setVisibility(View.VISIBLE);
        }*/
        list.setAdapter(adapter);
        MainActivity.setVisibilityForFilter();
        ((MainActivity)getActivity()).hideKeyworld();
//        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Service.INPUT_METHOD_SERVICE);
//        imm.showSoftInput(MainActivity.filter_top, 0);
        list.setBackgroundColor(Color.WHITE);
        list.setDivider(new ColorDrawable(0x99999999));
        list.setDividerHeight(1);
        final Gson gson = new Gson();
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Check if no view has focus:
                View view1 = getActivity().getCurrentFocus();
                if (view1 != null) {
                    //InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    ((MainActivity)getActivity()).imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                Preferences.get().putString(Preferences.RESTAURANT, gson.toJson(restaurants.get(position), Restaurant.class));
                AppController.currentRestaurantID = restaurants.get(position).getId();
                MainActivity.fragmentManager.beginTransaction()
                        .replace(R.id.container, Fragment.instantiate(getActivity(), "com.example.stillhot.app.Fragments.RestaurantFragmentDishes")) //"com.example.stillhot.app.pojo.MenuFragment"
                        .commit();

            }
        });
        android.widget.TextView filter = (android.widget.TextView) root.findViewById(R.id.textView32);
        filter.setVisibility(View.VISIBLE);
        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View view1 = getActivity().getCurrentFocus();
                if (view1 != null) {
                    //InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    ((MainActivity)getActivity()).imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                MainActivity.fragmentManager.beginTransaction()
                        .replace(R.id.container, Fragment.instantiate(getActivity(), "com.example.stillhot.app.Fragments.FilterFragment"))
                        .commit();
            }
        });
        final City city = gson.fromJson(Preferences.get().getString(Preferences.CITY), City.class);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Addresses.restlist
                + "city=" + city.getId()
                + "&rubric=" + AppController.currentRubric
                + "&category=" + AppController.currentCategory,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        RestObj restObj = gson.fromJson(response, RestObj.class);
                        if (restObj != null) {
                            Restaurant[] categories = restObj.getRestaurants();
                            if (categories != null) {
                                restaurants.addAll(Arrays.asList(categories));
                                doRestFilter(restaurants);
                                restaurants_temp.clear();
                                restaurants_temp.addAll(restaurants);
                                progressBar.setVisibility(View.GONE);
                                adapter.notifyDataSetChanged();
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String errore = "";
                        String msg = error.getMessage();
                    }
                });
        Connection.getInstance(getActivity().getApplicationContext()).addToRequestQueue(stringRequest, RestaurantFragment.class.getName());

        MainActivity.filter_top.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String editText = s.toString().toUpperCase();
                final ArrayList<Restaurant> restaurants_temp_custom = new ArrayList<>();
                if (editText.length() > 0) {
                    for (int i = 0; i < restaurants_temp.size(); i++) {
                        String title = restaurants_temp.get(i).getTitle();
                        if (title.length() >= editText.length()) {
                            String subName = restaurants_temp.get(i)
                                    .getTitle()
                                    .substring(0, editText.length())
                                    .toUpperCase();
                            if (editText.equals(subName)) {
                                restaurants_temp_custom.add(restaurants_temp.get(i));
                            }
                        }
                    }
                    restaurants.clear();
                    restaurants.addAll(restaurants_temp_custom);

                } else {
                    restaurants.clear();
                    restaurants.addAll(restaurants_temp);

                }
                doRestFilter(restaurants);
                adapter.notifyDataSetChanged();
            }
        });
        return root;
    }

    private void doRestFilter(ArrayList<Restaurant> restaurants) {
        int minSum = AppController.minSum;
        if (minSum != 0) {
            for (int i = 0; i < restaurants.size(); i++) {
                if (restaurants.get(i).getPrice_min() >= minSum) {
                    restaurants.remove(i);
                }
            }
        }
        Collections.sort(restaurants, (AppController.sort == 1) ? new RestaurantComparatorRatio() : new RestaurantComparatorName());
    }

    private class RestaurantComparatorName implements java.util.Comparator<Restaurant> {
        @Override
        public int compare(Restaurant restaurant, Restaurant t1) {
            return restaurant.getTitle().compareTo(t1.getTitle());
        }
    }

    private void setBasketVisibility(int intCount) {
        if (intCount == 0) {
            basket.setVisibility(View.GONE);
            count.setVisibility(View.GONE);
        } else {
            basket.setVisibility(View.VISIBLE);
            count.setVisibility(View.VISIBLE);
            count.setText("" + intCount);
        }
    }

    private class RestaurantComparatorRatio implements java.util.Comparator<Restaurant> {
        @Override
        public int compare(Restaurant restaurant, Restaurant t1) {
            int thisVal = restaurant.getRatio();
            int anotherVal = t1.getRatio();
            return (thisVal < anotherVal ? 1 : (thisVal == anotherVal ? 0 : -1));
        }
    }
}