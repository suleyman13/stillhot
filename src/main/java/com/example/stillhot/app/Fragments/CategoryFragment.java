package com.example.stillhot.app.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.stillhot.app.ListAdapterCategory;
import com.example.stillhot.app.MainActivity;
import com.example.stillhot.app.R;
import com.example.stillhot.app.back.*;
import com.example.stillhot.app.back.maps.Category;
import com.example.stillhot.app.back.maps.CategoryObj;
import com.example.stillhot.app.back.maps.City;
import com.example.stillhot.app.pojo.FourItemsListActivity;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Admin on 11.10.2015.
 */
public class CategoryFragment extends FourItemsListActivity {
    ListAdapterCategory adapter;
    ListView list;
    android.widget.RelativeLayout relativeLayout;
    ProgressBar progressBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final ViewGroup root = (ViewGroup) inflater.inflate(R.layout.activity_four_items_list, null);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        MainActivity.currentFragment = "com.example.stillhot.app.Fragments.CategoryFragment";
        MainActivity.setVisibilityForLogo();
        progressBar = (ProgressBar) root.findViewById(R.id.progressBar);
        relativeLayout = (android.widget.RelativeLayout) root.findViewById(R.id.rl_bottom);
        relativeLayout.setVisibility(View.GONE);
        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int stHeight = getStatusBarHeight() + getToolbarHeight();
        float ht_px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics());
        final int height = metrics.heightPixels - stHeight - (int)ht_px;
        list = (ListView) root.findViewById(R.id.listView);
        final ArrayList<Category> categories = new ArrayList<>();
        adapter = new ListAdapterCategory(this, getActivity(), R.layout.four_list_item, categories, height);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Category category = categories.get(position);
                AppController.currentCategory = category.getId();
                MainActivity.fragmentManager.beginTransaction()
                        .replace(R.id.container, Fragment.instantiate(getActivity(), "com.example.stillhot.app.Fragments.RestaurantFragment"))
                        .commit();
            }
        });
        final Gson gson = new Gson();
        final City city = gson.fromJson(Preferences.get().getString(Preferences.CITY), City.class);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Addresses.categories
                + "city=" + city.getId()
                + "&rubric=" + AppController.currentRubric,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        CategoryObj categoryObj = gson.fromJson(response, CategoryObj.class);
                        if (categoryObj != null) {
                            Category[] categoris = categoryObj.getCategories();
                            if (categoris != null) {
                                categories.addAll(Arrays.asList(categoris));
                                progressBar.setVisibility(View.GONE);
                                adapter.notifyDataSetChanged();
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String errore = "";
                        String msg = error.getMessage();
                    }
                });
        Connection.getInstance(getActivity().getApplicationContext()).addToRequestQueue(stringRequest, CategoryFragment.class.getName());
        return root;
    }
}