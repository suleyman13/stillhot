package com.example.stillhot.app.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.stillhot.app.MainActivity;
import com.example.stillhot.app.R;
import com.example.stillhot.app.back.Addresses;
import com.example.stillhot.app.back.Connection;
import com.example.stillhot.app.back.Preferences;
import com.example.stillhot.app.back.maps.City;
import com.example.stillhot.app.back.maps.RestObjSingle;
import com.example.stillhot.app.back.maps.Restaurant;
import com.example.stillhot.app.back.maps.Reviews;
import com.example.stillhot.app.pojo.BaseFragment;
import com.example.stillhot.app.pojo.ObservableScrollView;
import com.example.stillhot.app.pojo.ObservableScrollViewCallbacks;
import com.example.stillhot.app.pojo.ScrollUtils;
import com.google.gson.Gson;


/**
 * Created by Admin on 24.10.2015.
 */
public class RestFeedBack extends BaseFragment {
    public static final String ARG_SCROLL_Y = "ARG_SCROLL_Y";
    TextView tv_leavFeedBack;
    View line;
    ProgressBar progressBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_scrollview, container, false);
        final LinearLayout root = (LinearLayout) view.findViewById(R.id.root_layout);
        final ViewGroup content = (ViewGroup) inflater.inflate(R.layout.feedback, null);
        tv_leavFeedBack = (TextView) content.findViewById(R.id.textView52);
        line = content.findViewById(R.id.view_line);
        progressBar = (ProgressBar)content.findViewById(R.id.progressBar2);
        final ObservableScrollView scrollView = (ObservableScrollView) view.findViewById(R.id.scroll);
        Fragment parentActivity = getParentFragment();
        if (parentActivity instanceof ObservableScrollViewCallbacks) {
            // Scroll to the specified offset after layout
            Bundle args = getArguments();
            if (args != null && args.containsKey(ARG_SCROLL_Y)) {
                final int scrollY = args.getInt(ARG_SCROLL_Y, 0);
                ScrollUtils.addOnGlobalLayoutListener(scrollView, new Runnable() {
                    @Override
                    public void run() {
                        scrollView.scrollTo(0, scrollY);
                    }
                });
            }

            // TouchInterceptionViewGroup should be a parent view other than ViewPager.
            // This is a workaround for the issue #117:
            // https://github.com/ksoichiro/Android-ObservableScrollView/issues/117
            scrollView.setTouchInterceptionViewGroup((ViewGroup) parentActivity.getView().findViewById(R.id.root));
            scrollView.setScrollViewCallbacks((ObservableScrollViewCallbacks) parentActivity);
        }

        final Gson gson = new Gson();
        City city = gson.fromJson(Preferences.get().getString(Preferences.CITY), City.class);
        Restaurant restaurant = gson.fromJson(Preferences.get().getString(Preferences.RESTAURANT), Restaurant.class);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Addresses.restfeedback
                + "city=" + city.getId()
                + "&rest=" + restaurant.getId(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        RestObjSingle restObj = gson.fromJson(response, RestObjSingle.class);
                        if (restObj != null){
                            Reviews[] restRew = restObj.getReviews();
                            if(restRew != null) {
                                addFeedBackView(root, restRew);
                            }
                        }
                        setVisibilityForContent();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        Connection.getInstance(getActivity().getApplicationContext()).addToRequestQueue(stringRequest, RestFeedBack.class.getName());
        TextView leave_feedback = (TextView) content.findViewById(R.id.textView52);
        leave_feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.fragmentManager.beginTransaction()
                        .replace(R.id.container, Fragment.instantiate(getActivity(), "com.example.stillhot.app.Fragments.RestFeedBackSend"))
                        .commit();
            }
        });
        root.addView(content);
        return view;
    }

    private void setVisibilityForContent() {
        tv_leavFeedBack.setVisibility(View.VISIBLE);
        line.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);

    }

    private void addFeedBackView(LinearLayout lay, Reviews[] restRew) {
        for (int i = 0; i < restRew.length; i++) {
            View feed_item = View.inflate(getActivity(), R.layout.feedback_item, null);
            TextView author = (TextView) feed_item.findViewById(R.id.textView53);
            TextView message = (TextView) feed_item.findViewById(R.id.textView54);
            TextView date = (TextView) feed_item.findViewById(R.id.textView55);
            TextView time = (TextView) feed_item.findViewById(R.id.textView56);

            ImageView imageView = (ImageView)feed_item.findViewById(R.id.imageView4);
            imageView.setImageResource(restRew[i].getRating() == 1 ? R.drawable.yes : R.drawable.no);

            author.setText(restRew[i].getAuthor());
            message.setText(restRew[i].getMessage());
            date.setText(restRew[i].getDate());
            time.setText(restRew[i].getTime());
            lay.addView(feed_item);
        }
    }
}
