package com.example.stillhot.app.Fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.*;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.stillhot.app.MainActivity;
import com.example.stillhot.app.R;
import com.example.stillhot.app.back.Addresses;
import com.example.stillhot.app.back.Connection;
import com.example.stillhot.app.back.Preferences;
import com.example.stillhot.app.back.maps.City;
import com.example.stillhot.app.back.maps.Restaurant;

import com.google.gson.Gson;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Admin on 24.10.2015.
 */
public class RestFeedBackSend extends Fragment { //BaseFragment
    public static final String ARG_SCROLL_Y = "ARG_SCROLL_Y";
    boolean isGood = true;
    private boolean isSend = false;
    ViewGroup content;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        content = (ViewGroup) inflater.inflate(R.layout.feedback_send, null);
        MainActivity.currentFragment = "com.example.stillhot.app.Fragments.RestFeedBackSend";

        final ImageView yes = (ImageView) content.findViewById(R.id.imageView13);
        final ImageView no = (ImageView) content.findViewById(R.id.imageView12);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isGood) {
                    yes.setBackgroundResource(R.drawable.feedback_yes_green_put);
                    yes.setImageResource(R.drawable.yes_white);
                    no.setBackgroundResource(R.color.transparent);
                    no.setImageResource(R.drawable.no);
                    isGood = true;
                }
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isGood) {
                    yes.setBackgroundResource(R.color.transparent);
                    yes.setImageResource(R.drawable.yes);
                    no.setBackgroundResource(R.drawable.feedback_no_green_put);
                    no.setImageResource(R.drawable.no_white);
                    isGood = false;
                }
            }
        });

        TextView sendFeedBack = (TextView) content.findViewById(R.id.textView65);
        sendFeedBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                send();
            }
        });
        getActivity()
                .getWindow()
                .setSoftInputMode(
                        WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN |
                                WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        return content;
    }

    public void send() {
        if (!isSend) {
            final Gson gson = new Gson();
            final City city = gson.fromJson(Preferences.get().getString(Preferences.CITY), City.class);
            final Restaurant restaurant = gson.fromJson(Preferences.get().getString(Preferences.RESTAURANT), Restaurant.class);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Addresses.restfeedbackpost,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean result = jsonObject.getBoolean("result");
                                if (result) {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                    builder.setTitle("Отзыв отправлен");
                                    //builder.setMessage("Мы угадали ваш город?");
                                    builder.setCancelable(false);
                                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            RestaurantFragmentDishes f = new RestaurantFragmentDishes();
                                            // Supply index input as an argument.
                                            Bundle args = new Bundle();
                                            args.putBoolean("feedBack", true);
                                            f.setArguments(args);
                                            MainActivity.fragmentManager.beginTransaction()
                                                    .replace(R.id.container, f)
                                                    .commit();
                                        }
                                    })
                                            .show();
                                } else {
                                    Toast.makeText(getActivity(), "Отзыв не был отправлен!", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            isSend = false;
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            isSend = false;
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("city", "" + city.getId());
                    params.put("rest", "" + restaurant.getId());
                    if (!true) { //user.getId() != null

                    } else {
                        params.put("user", "0");
                        EditText author = (EditText) content.findViewById(R.id.textView60);
                        EditText phone = (EditText) content.findViewById(R.id.editText);
                        params.put("author", "" + author.getText());
                        params.put("phone", "" + phone.getText());
                    }
                    EditText message = (EditText) content.findViewById(R.id.editText2);
                    params.put("message", "" + message.getText());
                    params.put("rating", "" + (isGood ? 1 : 0));
                    return params;
                }
            };
            Connection.getInstance(getActivity().getApplicationContext()).addToRequestQueue(stringRequest, RestFeedBackSend.class.getName());
            isSend = true;
        }
    }
}
