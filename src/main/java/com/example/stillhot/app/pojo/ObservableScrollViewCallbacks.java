package com.example.stillhot.app.pojo;



/**
 * Created by Ruslan on 27.09.2015.
 */
public interface ObservableScrollViewCallbacks {
    void onScrollChanged(int var1, boolean var2, boolean var3);

    void onDownMotionEvent();

    void onUpOrCancelMotionEvent(ScrollState var1);
}
