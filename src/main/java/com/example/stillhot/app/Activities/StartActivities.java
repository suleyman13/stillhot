package com.example.stillhot.app.Activities;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.*;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.NumberPicker;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.stillhot.app.R;
import com.example.stillhot.app.back.*;
import com.example.stillhot.app.back.MyLocationListener;
import com.example.stillhot.app.back.maps.CitiesList;
import com.example.stillhot.app.back.maps.City;
import com.example.stillhot.app.back.maps.OSMAddress;
import com.example.stillhot.app.back.maps.OSMLocation;
import com.example.stillhot.app.MainActivity;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class StartActivities extends AppCompatActivity {

    BroadcastReceiver br = null;
    IntentFilter intFilter = null;
    public static String BROADCAST_ACTION = "com.example.stillhot.app.Activities.broadcast";
    boolean timeOut = false, isLocate = false;
    Timer timer;
    Dialog dialog;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start);
        getWindow().getDecorView().setBackgroundColor(getResources().getColor(R.color.background_dark));

        String city = Preferences.get().getString(Preferences.CITY);
        if (!TextUtils.isEmpty(city)) {
            Intent intent = new Intent(StartActivities.this, MainActivity.class);
            startActivity(intent);
            finish();
        } else {
            getCitiesFormNet();
            // создаем BroadcastReceiver
            br = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    if (!timeOut) {
                        Double latitude = intent.getDoubleExtra("Latitude", 0.0);
                        Double longitude = intent.getDoubleExtra("Longitude", 0.0);
                        // Определение города по координатам
                        getCityFromNominatim(String.valueOf(latitude), String.valueOf(longitude));
                    }
                }
            };

            // Создаем фильтр для BroadcastReceiver
            intFilter = new IntentFilter(BROADCAST_ACTION);
            // Регистрируем BroadcastReceiver
            registerReceiver(br, intFilter);
        }
    }

    private void getCityFromNominatim(String lat, String lon) {
        String url = Addresses.ReverseGeocode + "&lat=" + lat + "&lon=" + lon + "&addressdetails=1";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        timer.cancel();
                        final Gson gson = new Gson();
                        OSMLocation osmLocation = gson.fromJson(s, OSMLocation.class);
                        if (osmLocation != null) {
                            final OSMAddress address = osmLocation.getAddress();
                            if (address != null) {
                                final String city = address.getCity();
                                if (address.isCityInList(city)) {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(StartActivities.this);
                                    builder.setTitle(city);
                                    builder.setMessage("Мы угадали ваш город?");
                                    builder.setCancelable(false);
                                    builder.setPositiveButton("Да", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Preferences.get().putString(Preferences.CITY, gson.toJson(address.getCityClass(city), City.class));
                                            Intent intent = new Intent(StartActivities.this, MainActivity.class);
                                            startActivity(intent);
                                            finish();
                                        }
                                    }).setNegativeButton("Нет", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            openListCity();
                                        }
                                    }).show();
                                } else {
                                    openListCity();
                                }
                                isLocate = true;
                            }
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                    }
                }
        );
        Connection.getInstance(
                getApplicationContext()
        ).
                addToRequestQueue(stringRequest, StartActivities.class.getName()
                );
    }

    private void openListCity() {
        final Gson gson = new Gson();
        final CitiesList citiesList = gson.fromJson(Preferences.get().getString(Preferences.CITYLIST), CitiesList.class);
        final String[] genders = citiesList.getStringArray(); //{"Краснодар", "Алматы", "Ижевск", "Саратов", "Тюмень"};

        LayoutInflater ltInflater = getLayoutInflater();
        final View mView = ltInflater.inflate(R.layout.city_selector, null, false);

        NumberPicker cityPicker = (NumberPicker) mView.findViewById(R.id.cityPicker);
        cityPicker.setMinValue(0);
        cityPicker.setMaxValue(genders.length - 1);
        cityPicker.setDisplayedValues(genders);
        cityPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        final String[] choice = {genders[0]};
        NumberPicker.OnValueChangeListener myValueChangedListener = new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                // txtPickerOutput.setText("Value: " + genders[newVal]);
                choice[0] = genders[newVal];
            }
        };
        Button button = (Button) mView.findViewById(R.id.bt_choice_city);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                City city = citiesList.getCityFromArray(choice[0]);
                Preferences.get().putString(Preferences.CITY, gson.toJson(city, City.class));
                dialog.dismiss();
                Intent intent = new Intent(StartActivities.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialog = new Dialog(StartActivities.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(mView);
                dialog.setCancelable(false);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.show();
            }
        });


        cityPicker.setOnValueChangedListener(myValueChangedListener);
//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setView(mView).setInverseBackgroundForced(true);
//        dialog = new AlertDialog.Builder(this).setView(mView).show();
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        //trackEventViaGoogleAnalytics("Action", "CustomStringPicker");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (br != null) {
            unregisterReceiver(br);
        }
    }

    public void getCitiesFormNet() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Addresses.cities,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Gson gson = new Gson();
                        CitiesList citiesList = gson.fromJson(response, CitiesList.class);
                        if (citiesList != null) {
                            ArrayList<City> city = citiesList.getCities();
                            if (city != null && city.size() != 0) {
                                Preferences.get().putString(Preferences.CITYLIST, gson.toJson(citiesList));
                                timer = new Timer();
                                timer.schedule(new TimerTask() {
                                    @Override
                                    public void run() {
                                        timeOut = true;
                                        if (!isLocate)
                                            openListCity();
                                    }
                                }, 30000); // 30 cек для определения города по координатам
                                MyLocationListener.SetUpLocationListener(StartActivities.this);
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        getCitiesFormNet();
                    }
                });
        Connection.getInstance(getApplicationContext()).addToRequestQueue(stringRequest, StartActivities.class.getName());
    }
}
