package com.example.stillhot.app;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.example.stillhot.app.back.maps.Reviews;

import java.util.ArrayList;

/**
 * Created by Admin on 26.10.2015.
 */
public class FeedBackAdapter extends BaseAdapter {
    Context context;
    ArrayList<Reviews> arrayList;

    public FeedBackAdapter(Context context, ArrayList<Reviews> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Reviews getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null){
            LayoutInflater inflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.feedback_item, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.autor = (TextView) convertView.findViewById(R.id.textView53);
            viewHolder.message = (TextView) convertView.findViewById(R.id.textView54);
            viewHolder.date = (TextView) convertView.findViewById(R.id.textView54);
            viewHolder.time = (TextView) convertView.findViewById(R.id.textView54);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.autor.setText(getItem(position).getAuthor());
        viewHolder.message.setText(getItem(position).getMessage());
        viewHolder.date.setText(getItem(position).getDate());
        viewHolder.time.setText(getItem(position).getTime());

        return convertView;
    }

    class ViewHolder{
        TextView autor, date, time, message;

    }

}

