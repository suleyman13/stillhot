package com.example.stillhot.app.back;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Admin on 10.10.2015.
 */
public class RubricObject {
    @SerializedName("rubrics")
    Rubrica[] rubricas;

    public Rubrica[] getRubricas() {
        return rubricas;
    }
}
