package com.example.stillhot.app.back.maps;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 05.11.2015.
 */
public class RestAddress {
    @SerializedName("address")
    String address;

    public String getAddress() {
        return address;
    }
}
