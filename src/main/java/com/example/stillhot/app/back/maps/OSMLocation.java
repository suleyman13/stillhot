package com.example.stillhot.app.back.maps;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 04.10.2015.
 */
public class OSMLocation {
    @SerializedName("address")
    OSMAddress address;

    public OSMAddress getAddress() {
        return address;
    }
}
