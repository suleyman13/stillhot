package com.example.stillhot.app.back.maps;

import com.google.gson.annotations.SerializedName;

/**
 * Created by dev on 14.10.2015.
 */
public class Restaurant {
    @SerializedName("id")
    int id;

    @SerializedName("cities")
    int cities;

    @SerializedName("title")
    String title;

    @SerializedName("desc")
    String desc;

    @SerializedName("price_min")
    int price_min;

    @SerializedName("delivery_free")
    boolean delivery_free;

    @SerializedName("delivery_time")
    String delivery_time;

    @SerializedName("delivery_time_start")
    int delivery_time_start;

    @SerializedName("delivery_time_stop")
    int delivery_time_stop;

    @SerializedName("logo")
    String logo;

    @SerializedName("categories")
    int[] categories;

    @SerializedName("payments_online")
    boolean payments_online;

    @SerializedName("ratio")
    int ratio;

    @SerializedName("app_time")
    String app_time;

    @SerializedName("app_desc")
    String app_desc;

    @SerializedName("app_content")
    String app_content;



    public String getApp_time() {
        return app_time;
    }

    public String getApp_desc() {
        return app_desc;
    }

    public String getApp_content() {
        return app_content;
    }

    public int getId() {
        return id;
    }

    public int getCities() {
        return cities;
    }

    public String getTitle() {
        return title;
    }

    public String getDesc() {
        return desc;
    }

    public int getPrice_min() {
        return price_min;
    }

    public boolean isDelivery_free() {
        return delivery_free;
    }

    public String getDelivery_time() {
        return delivery_time;
    }

    public int getDelivery_time_start() {
        return delivery_time_start;
    }

    public int getDelivery_time_stop() {
        return delivery_time_stop;
    }

    public String getLogo() {
        return logo;
    }

    public int[] getCategories() {
        return categories;
    }

    public boolean isPayments_online() {
        return payments_online;
    }

    public int getRatio() {
        return ratio;
    }
}
