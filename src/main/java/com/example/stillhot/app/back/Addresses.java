package com.example.stillhot.app.back;

/**
 * Created by Admin on 04.10.2015.
 */
public class Addresses {
    public static final String ReverseGeocodeExample = "http://nominatim.openstreetmap.org/reverse?format=json&lat=45.0448400&lon=38.9760300&addressdetails=1";
    public static final String ReverseGeocode = "http://nominatim.openstreetmap.org/reverse?format=json";
    public static final String cities = "http://stillhot.ru/api/-action/getCities.json";
    public static final String rubrics = "http://stillhot.ru/api/-action/getRubrics.json?";
    public static final String categories = "https://stillhot.ru/api/:getCategories.json?"; // city=1&rubric=1
    public static final String restlist = "https://stillhot.ru/api/:getRestList.json?"; // city=1&rubric=1
    public static final String restmenu = "https://stillhot.ru/api/:getRestMenu.json?"; // city=1&rest=2
    public static final String restinfo = "https://stillhot.ru/api/:getRestInfo.json?"; // city=1&rest=2
    public static final String restfeedback = "https://stillhot.ru/api/:getRestReviews.json?"; // city=1&rest=2
    public static final String restfeedbackpost = "https://stillhot.ru/api/:postRestReviews.json"; // city=1&rest=2
    public static String postamr = "http://allcafefinal.ru/my/uploadfile.php";
}