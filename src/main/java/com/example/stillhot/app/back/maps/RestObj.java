package com.example.stillhot.app.back.maps;

import com.google.gson.annotations.SerializedName;

/**
 * Created by dev on 14.10.2015.
 */
public class RestObj {
    @SerializedName("rest")
    Restaurant[] restaurants;

    public Restaurant[] getRestaurants() {
        return restaurants;
    }

}
