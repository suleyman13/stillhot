package com.example.stillhot.app.back.maps;

import android.text.TextUtils;
import com.example.stillhot.app.back.Preferences;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Admin on 04.10.2015.
 */
public class OSMAddress {
    @SerializedName("city")
    String city;

    @SerializedName("town")
    String town;

    public String getCity() {
        return !TextUtils.isEmpty(city) ? city : town;
    }

    public City getCityClass(String city_from_provider) {
        Gson gson = new Gson();
        String str_cities = Preferences.get().getString(Preferences.CITYLIST);
        CitiesList citiesList = gson.fromJson(str_cities, CitiesList.class);
        City flag = null;
        if (citiesList != null) {
            ArrayList<City> cities = citiesList.getCities();
            for (int i = 0; i < cities.size(); i++) {
                if (cities.get(i).getTitle().equals(city_from_provider)) {
                    flag = cities.get(i);
                    break;
                }
            }
        }
        return flag;
    }

    public boolean isCityInList(String city_from_provider) {
        Gson gson = new Gson();
        String str_cities = Preferences.get().getString(Preferences.CITYLIST);
        CitiesList citiesList = gson.fromJson(str_cities, CitiesList.class);
        if (citiesList != null) {
            boolean flag = false;
            ArrayList<City> cities = citiesList.getCities();
            for (int i = 0; i < cities.size(); i++) {
                if (cities.get(i).getTitle().equals(city_from_provider)) {
                    flag = true;
                    break;
                } else
                    flag = false;
            }
            return flag;
        } else {
            return false;
        }
    }
}
