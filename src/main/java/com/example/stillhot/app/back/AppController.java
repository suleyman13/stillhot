package com.example.stillhot.app.back;

import android.app.Application;
import android.graphics.drawable.ColorDrawable;

import java.util.HashMap;

public class AppController extends Application {

    public static int sort = 1;
    public static int minSum = 0;
    public static int currentRestaurantID = -1;

    @Override
    public void onCreate() {
        super.onCreate();
        Connection.init(getApplicationContext());
        Preferences.init(getApplicationContext());
        Imageloader.init(getApplicationContext());
        Imageloader.get().setPlaceholder(new ColorDrawable(0x0000));
    }

    public static int currentRubric = 0;
    public static int currentCategory = 0;
    public static int rubricCount = 0;

    public static HashMap<Integer, Integer> basketMap = new HashMap<>();
}
