package com.example.stillhot.app.back.maps;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Admin on 07.10.2015.
 */
public class CitiesList {
    @SerializedName("cities")
    ArrayList<City> cities;

    public ArrayList<City> getCities() {
        return cities;
    }

    public String[] getStringArray() {
        if (cities != null) {
            String[] arr = new String[cities.size()];
            for (int i = 0; i < cities.size(); i++)
                arr[i] = cities.get(i).getTitle();
            return arr;
        } else {
            return new String[]{""};
        }
    }

    public City getCityFromArray(String city_str) {
        City city = null;
        for (int i = 0; i < cities.size(); i++)
            if (cities.get(i).getTitle().equals(city_str))
                city = cities.get(i);
        return city;
    }
}
