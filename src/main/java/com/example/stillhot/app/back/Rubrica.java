package com.example.stillhot.app.back;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 09.10.2015.
 */
public class Rubrica {

    @SerializedName("id")
    private int id;

    @SerializedName("title")
    private String title;

    @SerializedName("icon")
    private String icon;

    @SerializedName("icon_menu")
    private String icon_menu;

    @SerializedName("categories")
    private int[] categories;


    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getIcon() {
        return icon;
    }

    public String getIcon_menu() {
        return icon_menu;
    }

    public int[] getCategories() {
        return categories;
    }
}
