package com.example.stillhot.app.back.maps;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 19.10.2015.
 */
public class DishObj {

    @SerializedName("dishes")
    Dish[] dish;

    // Масссив блюд ресторана, связь с категорией
    public Dish[] getDish() {
        return dish;
    }
}
