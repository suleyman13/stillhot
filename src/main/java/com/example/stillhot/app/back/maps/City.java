package com.example.stillhot.app.back.maps;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 07.10.2015.
 */
public class City {

    @SerializedName("id")
    private int id;

    @SerializedName("title")
    private String title;

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }
}
