package com.example.stillhot.app.back.maps;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 26.10.2015.
 */
public class RestObjSingle {
    @SerializedName("rest")
    Restaurant restaurant;

    @SerializedName("reviews")
    Reviews[] reviews;

    @SerializedName("rest_addr")
    RestAddress[] rest_addr;

    public RestAddress[] getRest_addr() {
        return rest_addr;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public Reviews[] getReviews() {
        return reviews;
    }
}
