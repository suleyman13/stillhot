package com.example.stillhot.app.back;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v4.app.FragmentActivity;
import com.example.stillhot.app.back.maps.Dish;
import com.example.stillhot.app.back.maps.OSMLocation;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by dev on 17.08.2015.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    final String LOG_TAG = "MADYAR SQL_TAG";

    static DatabaseHelper databaseHelper;
    static String tableNameSetting = "settings";
    static String tableNameDishesBasket = "dishes_basket";

    public synchronized static DatabaseHelper getInit(Context context) {
        if (databaseHelper == null)
            databaseHelper = new DatabaseHelper(context);
        return databaseHelper;
    }

    public DatabaseHelper(Context context) {
        super(context, "localDB", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String script = "CREATE TABLE " + tableNameSetting + " (" +
                "id integer primary key autoincrement, " +
                "name text);";
        db.execSQL(script);

        script = "CREATE TABLE " + tableNameDishesBasket + " (" +
                "id integer primary key autoincrement, " +
                "dish_id integer," +
                "count integer," +
                "price integer);";
        db.execSQL(script);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    /*public static void putToDb(final Context context, final String str) {
        Gson gson = new Gson();
        OSMLocation location = gson.fromJson(str, OSMLocation.class);
        if (location != null) {
            SQLiteDatabase wdb = new DatabaseHelper(context).getWritableDatabase();
            Cursor cursor = wdb.rawQuery("Select id_group from " + tableNameSetting, null);
            String cityName = "";
            if (cursor.moveToNext()) {
                cityName = cursor.getString(cursor.getColumnIndex("name"));
            }
            wdb.close();
        }
    }*/

    /*public static int getCountById(Context context, String table, long id) {
        SQLiteDatabase rdb = new DatabaseHelper(context).getReadableDatabase();
        StringBuilder sb = new StringBuilder("SELECT a.count FROM ").append(table).append(" AS a WHERE id_group =").append(id);
        Cursor cursor = rdb.rawQuery(sb.toString(), null);
        int count = 0;
        if (cursor.moveToNext())
            count = cursor.getInt(0);
        rdb.close();
        return count;
    }*/

    /*public static HashMap<String, Integer> getNewer(Context context) {
        SQLiteDatabase rdb = DatabaseHelper.getInit(context).getReadableDatabase();
        HashMap<String, Integer> hash = new HashMap<String, Integer>();
        for (int i = 0; i < 4; i++) {
            *//*String sql_request = "SELECT SUM(count) FROM " + tables[i];
            Cursor cursor = rdb.rawQuery(sql_request, null);
            if (cursor.moveToNext()) {
                int count = cursor.getInt(0);
                hash.put(tables[i], count);
            }*//*
        }
        //rdb.close();
        return hash;
    }*/

    /*public static void resetNewer(Context context, String table, long id) {
        SQLiteDatabase wdb = DatabaseHelper.getInit(context).getWritableDatabase();
        StringBuilder sb = new StringBuilder("DELETE FROM ").append(table).append(" WHERE id_group =").append(id);
        wdb.rawQuery(sb.toString(), null).moveToNext();
        wdb.close();
    }*/

    public static synchronized int getDishCount(Context context, int id) {
        DatabaseHelper db = new DatabaseHelper(context);
        SQLiteDatabase rdb = db.getReadableDatabase();//DatabaseHelper.getInit(context).getReadableDatabase();
        StringBuilder sb = new StringBuilder("SELECT count FROM ")
                .append(tableNameDishesBasket)
                .append(" WHERE dish_id =")
                .append(id);
        Cursor cursor = rdb.rawQuery(sb.toString(), null);
        int count = 0;
        if (cursor.moveToNext())
            count = cursor.getInt(0);
        rdb.close();
        return count;
    }

    public static int addDishCount(Context context, int id) {
        DatabaseHelper db = new DatabaseHelper(context);
        SQLiteDatabase wdb = db.getWritableDatabase();//DatabaseHelper.getInit(context).getWritableDatabase();
        StringBuilder sb = new StringBuilder("SELECT count FROM ")
                .append(tableNameDishesBasket)
                .append(" WHERE dish_id =")
                .append(id);
        Cursor cursor = wdb.rawQuery(sb.toString(), null);
        int count = 0;
        if (cursor.moveToNext())
            count = cursor.getInt(0);
        StringBuilder wsb = new StringBuilder("UPDATE ")
                .append(tableNameDishesBasket)
                .append(" SET count=count+1")
                .append(" WHERE dish_id=")
                .append(id);
        wdb.rawQuery(wsb.toString(), null).moveToNext();
        wdb.close();
        return count + 1;
    }

    public static int deductDishCount(Context context, int id) {
        DatabaseHelper db = new DatabaseHelper(context);
        SQLiteDatabase wdb = db.getWritableDatabase(); //DatabaseHelper.getInit(context).getWritableDatabase();
        StringBuilder sb = new StringBuilder("SELECT count FROM ").append(tableNameDishesBasket).append(" WHERE dish_id =").append(id);
        Cursor cursor = wdb.rawQuery(sb.toString(), null);
        int count = 0;
        if (cursor.moveToNext())
            count = cursor.getInt(0);
        if (count > 0) {
            StringBuilder wsb = new StringBuilder("UPDATE ")
                    .append(tableNameDishesBasket)
                    .append(" SET count=count-1")
                    .append(" WHERE dish_id=")
                    .append(id);
            wdb.rawQuery(wsb.toString(), null).moveToNext();
        }
        wdb.close();
        return count - 1;
    }

    public static void addDishes(Context context, Dish[] dishs) {
        HashSet<Integer> IDs = new HashSet<>();
        DatabaseHelper db = new DatabaseHelper(context);
        SQLiteDatabase wdb = db.getWritableDatabase(); //DatabaseHelper.getInit(context).getWritableDatabase();
        StringBuilder sb = new StringBuilder("SELECT dish_id FROM ").append(tableNameDishesBasket);
        Cursor cursor = wdb.rawQuery(sb.toString(), null);
        while (cursor.moveToNext())
            IDs.add(cursor.getInt(0));
        for (int i = 0; i < dishs.length; i++) {
            if (!IDs.contains(dishs[i].getId())){
                ContentValues values = new ContentValues();
                values.put("dish_id", dishs[i].getId());
                values.put("count", 0);
                values.put("price", dishs[i].getPrice());
                wdb.insert(tableNameDishesBasket, null, values);
            }
        }
        wdb.close();
    }
    public static int checkBasketCount(Context context) {
        DatabaseHelper db = new DatabaseHelper(context);
        SQLiteDatabase rdb = db.getReadableDatabase();//DatabaseHelper.getInit(context).getReadableDatabase();
        StringBuilder sb = new StringBuilder("SELECT sum(count) FROM ").append(tableNameDishesBasket);
        Cursor cursor = rdb.rawQuery(sb.toString(), null);
        int count = 0;
        if (cursor.moveToNext())
            count = cursor.getInt(0);
        rdb.close();
        return count;
    }

    public static HashMap<Integer, Integer> getDishNotCountZero(Context context) {
        HashMap<Integer, Integer> hashMap = new HashMap<Integer, Integer>();
        DatabaseHelper db = new DatabaseHelper(context);
        SQLiteDatabase rdb = db.getReadableDatabase();//DatabaseHelper.getInit(context).getReadableDatabase();
        StringBuilder sb = new StringBuilder("SELECT dish_id, count FROM ")
                .append(tableNameDishesBasket)
                .append(" WHERE count > 0");
        Cursor cursor = rdb.rawQuery(sb.toString(), null);
        while (cursor.moveToNext()){
            hashMap.put(cursor.getInt(0), cursor.getInt(1));
        }
        return hashMap;
    }


    public static int getSumBasket(Context context) {
        DatabaseHelper db = new DatabaseHelper(context);
        SQLiteDatabase rdb = db.getReadableDatabase();//DatabaseHelper.getInit(context).getReadableDatabase();
        StringBuilder sb = new StringBuilder("SELECT sum(count*price) FROM ")
                .append(tableNameDishesBasket)
                .append(" WHERE count > 0");
        Cursor cursor = rdb.rawQuery(sb.toString(), null);
        int sum = 0;
        if (cursor.moveToNext()){
            sum = cursor.getInt(0);
        }
        return sum;
    }

    public static HashMap<Integer, Integer> getDishCount(Context context) {
        HashMap<Integer, Integer> map = new HashMap();
        DatabaseHelper db = new DatabaseHelper(context);
        SQLiteDatabase rdb = db.getReadableDatabase();//DatabaseHelper.getInit(context).getReadableDatabase();
        StringBuilder sb = new StringBuilder("SELECT dish_id, count FROM ")
                .append(tableNameDishesBasket);
        Cursor cursor = rdb.rawQuery(sb.toString(), null);
        while (cursor.moveToNext())
            map.put(cursor.getInt(0), cursor.getInt(1));
        rdb.close();
        return map;
    }
}