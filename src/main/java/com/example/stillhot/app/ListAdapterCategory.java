package com.example.stillhot.app;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.example.stillhot.app.back.Rubrica;
import com.example.stillhot.app.back.maps.Category;
import com.example.stillhot.app.pojo.FourItemsListActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Admin on 11.10.2015.
 */
public class ListAdapterCategory extends ArrayAdapter<Category> {
    private FourItemsListActivity fourItemsListActivity;
    int res;
    float h;
    ArrayList<Category> rubricas;

    public ListAdapterCategory(FourItemsListActivity fourItemsListActivity, Context context, int resource, ArrayList<Category> rubricas, float h) {
        super(context, resource, rubricas);
        this.fourItemsListActivity = fourItemsListActivity;
        this.res = resource;
        this.rubricas = rubricas;
        this.h = h;
    }

    public ListAdapterCategory(FourItemsListActivity fourItemsListActivity, Context context, int resource, float height) {
        super(context, resource);
        this.fourItemsListActivity = fourItemsListActivity;
        this.res = resource;
        this.h = height;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.four_list_item, parent, false);
        ImageView image = (ImageView) convertView.findViewById(R.id.image);
        if (!TextUtils.isEmpty(rubricas.get(position).getIcon_app())) {
            Picasso.with(getContext()).load(rubricas.get(position).getIcon_app()).into(image);
        }
        TextView name = (TextView) convertView.findViewById(R.id.name);
        name.setText(rubricas.get(position).getTitle());
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) h / 6);
        image.setLayoutParams(params);
        return convertView;
    }
}
