package com.example.stillhot.app;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.example.stillhot.app.back.DatabaseHelper;
import com.example.stillhot.app.back.maps.Dish;
import com.example.stillhot.app.ui.MyTextViewBold;
import com.example.stillhot.app.ui.MyTextViewLight;

import java.util.ArrayList;

/**
 * Created by Admin on 11.10.2015.
 */
public class ListAdapterDish extends ArrayAdapter<Dish> {
    int res;
    ArrayList<Dish> dishes;
    RelativeLayout basketFloor;

    public ListAdapterDish(Context context, int resource, ArrayList<Dish> restaurants, RelativeLayout basketFloor) {
        super(context, resource, restaurants);
        this.res = resource;
        this.dishes = restaurants;
        this.basketFloor =basketFloor;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder viewHolder;

        if (convertView == null){
            LayoutInflater inflater = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(res, parent, false);// View.inflate(getContext(), res, null); //inflater.inflate(R.layout.list_item, viewGroup, false);
            viewHolder = new ViewHolder();
            viewHolder.image = (ImageView) convertView.findViewById(R.id.dish_image);
            viewHolder.name = (MyTextViewLight) convertView.findViewById(R.id.name);
            viewHolder.desc = (MyTextViewLight) convertView.findViewById(R.id.textView16);
            viewHolder.price = (MyTextViewLight) convertView.findViewById(R.id.textView7);
            viewHolder.tv_count = (MyTextViewBold) convertView.findViewById(R.id.textView17);
            viewHolder.tv_minus = (TextView) convertView.findViewById(R.id.textView19);
            viewHolder.tv_plus = (TextView) convertView.findViewById(R.id.textView18);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        /*if (!TextUtils.isEmpty(dishes.get(position).getPhoto())) {
            Picasso.with(getContext()).load(dishes.get(position).getPhoto()).into(viewHolder.image);
        }*/

        viewHolder.name.setText(dishes.get(position).getTitle());
        viewHolder.desc.setText(dishes.get(position).getDescr());
        viewHolder.price.setText("" + dishes.get(position).getPrice());
        int count = DatabaseHelper.getDishCount(getContext(), dishes.get(position).getId());
        if (count != 0) {
            viewHolder.tv_count.setVisibility(View.VISIBLE);
            viewHolder.tv_count.setText("" + count);
        } else {
            viewHolder.tv_count.setVisibility(View.INVISIBLE);
            viewHolder.tv_count.setText("0");
        }
        viewHolder.tv_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count = DatabaseHelper.deductDishCount(getContext(), dishes.get(position).getId());
                if (count <= 0) {
                    checkBasket();
                    viewHolder.tv_minus.setEnabled(false);
                    viewHolder.tv_count.setText("");
                    viewHolder.tv_count.setVisibility(View.INVISIBLE);
                } else {
                    viewHolder.tv_count.setText("" + count);
                    viewHolder.tv_count.setVisibility(View.VISIBLE);
                }
                checkBasket();
            }
        });

        viewHolder.tv_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewHolder.tv_count.setText("" + DatabaseHelper.addDishCount(getContext(), dishes.get(position).getId()));
                viewHolder.tv_count.setVisibility(View.VISIBLE);
                viewHolder.tv_minus.setEnabled(true);
                checkBasket();
            }
        });
        return convertView;
    }

    private synchronized void checkBasket() {
        int count = DatabaseHelper.checkBasketCount(getContext());
        TextView tv_basketCount = (TextView) basketFloor.findViewById(R.id.textView15);
        tv_basketCount.setText("" + count);
        basketFloor.setVisibility((count <= 0) ? View.GONE : View.VISIBLE);
    }

    class ViewHolder {
        MyTextViewLight name, desc, price;
        MyTextViewBold tv_count;
        TextView tv_minus, tv_plus;
        ImageView image;
    }
}
