package com.example.stillhot.app.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by Robot on 13.07.2015.
 */
public class AspectRatioImageView extends ImageView {
    private float ratio = 0.5f; //отношение высоты к ширине
    private boolean isRounded = false;
    public AspectRatioImageView(Context context, float ratio) {
        super(context);
        this.ratio = ratio;

    }
    public void setRatio(float rat){
        this.ratio = rat;
    }
    public void setRounded(boolean is){
        this.isRounded = is;

    }

    public AspectRatioImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AspectRatioImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = getMeasuredWidth();
        int height = Math.round(width * ratio);
        setMeasuredDimension(width, height);
    }
    @Override
    protected void onDraw(Canvas canvas) {
        if(isRounded) {

            float radius = 5.0f;
            Path clipPath = new Path();
            RectF rect = new RectF(0, 0, this.getWidth(), this.getHeight());
            clipPath.addRoundRect(rect, radius, radius, Path.Direction.CW);
            canvas.clipPath(clipPath);
        }
        super.onDraw(canvas);
    }
}
