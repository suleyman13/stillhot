package com.example.stillhot.app;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.*;
import android.view.inputmethod.InputMethodManager;
import com.example.stillhot.app.Fragments.RestaurantFragmentDishes;
import com.example.stillhot.app.back.AppController;
import com.example.stillhot.app.back.Preferences;
import com.example.stillhot.app.back.RubricObject;
import com.example.stillhot.app.back.Rubrica;
import com.example.stillhot.app.ui.MyTextViewLight;
import com.google.gson.Gson;
import android.widget.EditText;

import java.util.ArrayList;

/**
 * Created by Admin on 07.10.2015.
 */
public class MainActivity extends AppCompatActivity {
    public InputMethodManager imm;
    public static String currentFragment, preFragment;
    static String[] TITLES = {"Акции", "Контакты", "Настройки"};
    static String ICONS[] = {
            "file:///android_asset/personal_account.png",
            "file:///android_asset/slidebar_contects.png",
            "file:///android_asset/slidebar_settings_icon.png"};
    private Toolbar toolbar;                              // Declaring the Toolbar Object

    RecyclerView mRecyclerView;                           // Declaring RecyclerView
    static MyAdapter mAdapter;                            // Declaring Adapter For Recycler View
    RecyclerView.LayoutManager mLayoutManager;            // Declaring Layout Manager as a linear layout manager
    DrawerLayout Drawer;                                  // Declaring DrawerLayout

    final String[] fragments = {
            "com.example.stillhot.app.pojo.FourItemsListActivity",
            "com.example.stillhot.app.Fragments.CategoryFragment",
            "com.example.stillhot.app.Fragments.RestaurantFragment",
            "com.example.stillhot.app.Fragments.FilterFragment",
            "com.example.stillhot.app.Fragments.RestaurantFragmentDishes",
            "com.example.stillhot.app.Fragments.RestFeedBackSend"
    };

    ActionBarDrawerToggle mDrawerToggle;
    //Similarly we Create a String Resource for the name and email in the header view
    //And we also create a int resource for profile picture in the header view

    String NAME = "Akash Bangad";
    String EMAIL = "akash.bangad@android4devs.com";
    int PROFILE = R.drawable.original;
    public static FragmentManager fragmentManager;
    public static EditText filter_top;
    public static MyTextViewLight tv_basket;
    static android.widget.ImageView logo_tool;
    //static android.widget.RelativeLayout relativeLayout;
    private boolean wasKeyboardOpen = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navigation_drawer_layout);
        imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        getWindow().getDecorView().setBackgroundColor(getResources().getColor(R.color.black));
        Drawer = (DrawerLayout) findViewById(R.id.DrawerLayout);
        toolbar = (Toolbar) findViewById(R.id.toolbar_order);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setBackgroundColor(getResources().getColor(R.color.black));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_max);
        filter_top = (EditText) findViewById(R.id.toolbar_et);
        try {
            Drawer.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {

                    Rect r = new Rect();
                    Drawer.getWindowVisibleDisplayFrame(r);

                    int heightDiff = Drawer.getRootView().getHeight() - (r.bottom - r.top);
                    if (heightDiff > 100) {
                        wasKeyboardOpen = true;
                        // kEYBOARD IS OPEN

                    } else {
                        if (wasKeyboardOpen) {
                            wasKeyboardOpen = false;
                            // Do your toast here
                            filter_top.setCursorVisible(false);
                            filter_top.setHint("Поиск");
                        }
                        // KEYBOARD IS HIDDEN
                    }
                }
            });
        } catch (Throwable e) {
            e.printStackTrace();
        }
        filter_top.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                filter_top.setCursorVisible(true);
                filter_top.setHint("");
                filter_top.requestFocus();
                imm.showSoftInput(filter_top, 0);
                return true;
            }
        });
        logo_tool = (android.widget.ImageView) findViewById(R.id.toolbar_iv);
        tv_basket = (MyTextViewLight) findViewById(R.id.tv_basket);

        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, Fragment.instantiate(MainActivity.this, fragments[0]))
                .commit();

        mRecyclerView = (RecyclerView) findViewById(R.id.RecyclerView); // Assigning the RecyclerView Object to the xml View
        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this,
                        new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                Gson gson = new Gson();
                                String ro = Preferences.get().getString(Preferences.RUBRICOBJECT);
                                Rubrica[] rubricas;
                                int addedCount = 0;
                                int id = 0;
                                int categoryLength = 0;
                                int[] categories = {};
                                RubricObject rubrica = gson.fromJson(ro, RubricObject.class);
                                if (rubrica != null) {
                                    rubricas = rubrica.getRubricas();
                                    addedCount = rubricas.length;
                                    if (position <= addedCount - 1) {
                                        id = rubricas[position].getId();
                                        categoryLength = rubricas[position].getCategories().length;
                                        categories = rubricas[position].getCategories();
                                    }
                                }
                                if (position <= addedCount) {
                                    if (categoryLength > 1) {
                                        MainActivity.preFragment = "";
                                        AppController.currentRubric = id;
                                        MainActivity.fragmentManager.beginTransaction()
                                                .replace(R.id.container, Fragment.instantiate(MainActivity.this, "com.example.stillhot.app.Fragments.CategoryFragment"))
                                                .commit();
                                    } else if(categoryLength == 1){
                                        MainActivity.preFragment = "com.example.stillhot.app.pojo.FourItemsListActivity";
                                        AppController.currentRubric = id;
                                        AppController.currentCategory = categories[0];
                                        MainActivity.fragmentManager.beginTransaction()
                                                .replace(R.id.container, Fragment.instantiate(MainActivity.this, "com.example.stillhot.app.Fragments.RestaurantFragment"))
                                                .commit();
                                    }
                                } else {

                                }
                                Drawer.closeDrawer(Gravity.RIGHT);
                            }
                        })
        );
        mRecyclerView.setHasFixedSize(true);                            // Letting the system know that the list objects are of fixed size

        mAdapter = new MyAdapter(MainActivity.this, TITLES, ICONS, NAME, EMAIL, PROFILE);       // Creating the Adapter of MyAdapter class(which we are going to see in a bit)
        // And passing the titles,icons,header view name, header view email,
        // and header view profile picture

        mRecyclerView.setAdapter(mAdapter);                              // Setting the adapter to RecyclerView

        mLayoutManager = new LinearLayoutManager(this);                 // Creating a layout Manager

        mRecyclerView.setLayoutManager(mLayoutManager);



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case R.id.basket:
                if (Drawer.isDrawerOpen(Gravity.RIGHT)) {
                    Drawer.closeDrawer(Gravity.RIGHT);
                } else {
                    Drawer.openDrawer(Gravity.RIGHT);
                }
                return true;
            case android.R.id.home:
                if (currentFragment.equals("com.example.stillhot.app.pojo.FourItemsListActivity")) {
                    finish();
                } else if (currentFragment.equals("com.example.stillhot.app.Fragments.CategoryFragment")) {
                    fragmentManager.beginTransaction()
                            .replace(R.id.container, Fragment.instantiate(MainActivity.this, fragments[0]))
                            .commit();
                } else if (currentFragment.equals("com.example.stillhot.app.Fragments.RestaurantFragment")) {
                    String frag = "";
                    if (preFragment.equals("com.example.stillhot.app.pojo.FourItemsListActivity"))
                        frag = fragments[0];
                    else
                        frag = fragments[1];
                    fragmentManager.beginTransaction()
                            .replace(R.id.container, Fragment.instantiate(MainActivity.this, frag))
                            .commit();
                    View view = this.getCurrentFocus();
                    if (view != null) {
                        //InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                } else if (currentFragment.equals("com.example.stillhot.app.Fragments.FilterFragment")) {
                    fragmentManager.beginTransaction()
                            .replace(R.id.container, Fragment.instantiate(MainActivity.this, fragments[2]))
                            .commit();
                } else if (currentFragment.equals("com.example.stillhot.app.Fragments.RestaurantFragmentDishes")) {
                    fragmentManager.beginTransaction()
                            .replace(R.id.container, Fragment.instantiate(MainActivity.this, fragments[2]))
                            .commit();
                } else if (currentFragment.equals("com.example.stillhot.app.Fragments.RestFeedBackSend")) {
                    RestaurantFragmentDishes f = new RestaurantFragmentDishes();
                    // Supply index input as an argument.
                    Bundle args = new Bundle();
                    args.putBoolean("feedBack", true);
                    f.setArguments(args);
                    fragmentManager.beginTransaction()
                            .replace(R.id.container, f)
                            .commit();
                }
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    public static void addToSlideBar(ArrayList<Rubrica> rubrics) {
        String[] ICONSs = new String[rubrics.size() + 3];
        String[] TITLESs = new String[rubrics.size() + 3];
        for (int i = 0; i < rubrics.size(); i++) {
            ICONSs[i] = rubrics.get(i).getIcon_menu();
            TITLESs[i] = rubrics.get(i).getTitle();
        }
        for (int i = rubrics.size(); i < rubrics.size() + 3; i++) {
            ICONSs[i] = ICONS[i - rubrics.size()];
            TITLESs[i] = TITLES[i - rubrics.size()];
        }
        mAdapter.setIconsTitles(ICONSs, TITLESs);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {
        if (Drawer.isDrawerOpen(Gravity.RIGHT)) {
            Drawer.closeDrawer(Gravity.RIGHT);
        } else {
            if (currentFragment.equals("com.example.stillhot.app.pojo.FourItemsListActivity")) {
                finish();
            } else if (currentFragment.equals("com.example.stillhot.app.Fragments.CategoryFragment")) {
                fragmentManager.beginTransaction()
                        .replace(R.id.container, Fragment.instantiate(MainActivity.this, fragments[0]))
                        .commit();
            } else if (currentFragment.equals("com.example.stillhot.app.Fragments.RestaurantFragment")) {
                String frag = "";
                if (preFragment.equals("com.example.stillhot.app.pojo.FourItemsListActivity"))
                    frag = fragments[0];
                else
                    frag = fragments[1];
                fragmentManager.beginTransaction()
                        .replace(R.id.container, Fragment.instantiate(MainActivity.this, frag))
                        .commit();
                View view = this.getCurrentFocus();
                if (view != null) {
                    //InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }

            } else if (currentFragment.equals("com.example.stillhot.app.Fragments.FilterFragment")) {
                fragmentManager.beginTransaction()
                        .replace(R.id.container, Fragment.instantiate(MainActivity.this, fragments[2]))
                        .commit();
            } else if (currentFragment.equals("com.example.stillhot.app.Fragments.RestaurantFragmentDishes")) {
                fragmentManager.beginTransaction()
                        .replace(R.id.container, Fragment.instantiate(MainActivity.this, fragments[2]))
                        .commit();
            } else if (currentFragment.equals("com.example.stillhot.app.Fragments.RestFeedBackSend")) {
                RestaurantFragmentDishes f = new RestaurantFragmentDishes();
                // Supply index input as an argument.
                Bundle args = new Bundle();
                args.putBoolean("feedBack", true);
                f.setArguments(args);
                fragmentManager.beginTransaction()
                        .replace(R.id.container, f)
                        .commit();
            }
        }
    }

    public static void setVisibilityForFilter() {
        filter_top.setVisibility(View.VISIBLE);
        tv_basket.setVisibility(View.GONE);
        logo_tool.setVisibility(View.GONE);

    }

    public static void setVisibilityForBasket() {
        filter_top.setVisibility(View.GONE);
        tv_basket.setVisibility(View.VISIBLE);
        logo_tool.setVisibility(View.GONE);
    }

    public static void setVisibilityForLogo() {
        filter_top.setVisibility(View.GONE);
        tv_basket.setVisibility(View.GONE);
        logo_tool.setVisibility(View.VISIBLE);
    }

    public void hideKeyworld(){
        imm.hideSoftInputFromWindow(filter_top.getWindowToken(), 0);
        filter_top.clearFocus();
        //filter_top.setCursorVisible(false);
        MainActivity.filter_top.setText("");
        MainActivity.filter_top.setHint("Поиск");
        filter_top.setCursorVisible(false);
        //MainActivity.filter_top.clearFocus();
    }
}