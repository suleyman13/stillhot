package com.example.stillhot.app;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.stillhot.app.back.Imageloader;
import com.example.stillhot.app.back.maps.Restaurant;
import com.example.stillhot.app.ui.MyTextViewRoman;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Admin on 11.10.2015.
 */
public class ListAdapterRestaurant extends ArrayAdapter<Restaurant> {
    int res;
    ArrayList<Restaurant> restaurants;

    public ListAdapterRestaurant(Context context, int resource, ArrayList<Restaurant> restaurants) {
        super(context, resource, restaurants);
        this.res = resource;
        this.restaurants = restaurants;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null){
            LayoutInflater inflater = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(res, parent, false);// View.inflate(getContext(), res, null); //inflater.inflate(R.layout.list_item, viewGroup, false);
            viewHolder = new ViewHolder();
            viewHolder.image = (ImageView) convertView.findViewById(R.id.imageView7);
            viewHolder.name = (MyTextViewRoman) convertView.findViewById(R.id.textView20);
            viewHolder.minSum = (MyTextViewRoman) convertView.findViewById(R.id.textView22);
            viewHolder.deliverFree = (MyTextViewRoman) convertView.findViewById(R.id.textView66);
            viewHolder.DeliveryTime = (MyTextViewRoman) convertView.findViewById(R.id.textView68);
            viewHolder.ratio = (MyTextViewRoman) convertView.findViewById(R.id.textView72);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        /*if (!TextUtils.isEmpty(restaurants.get(position).getLogo())) {
            Picasso.with(getContext()).load(restaurants.get(position).getLogo()).into(viewHolder.image);
        }*/
        Imageloader.get().loadImage(restaurants.get(position).getLogo(), viewHolder.image);

        viewHolder.name.setText(restaurants.get(position).getTitle());
        viewHolder.minSum.setText("" + restaurants.get(position).getPrice_min());
        viewHolder.deliverFree.setText(restaurants.get(position).isDelivery_free() ? "Бесплатно" : "Платно");
        viewHolder.DeliveryTime.setText(restaurants.get(position).getDelivery_time() + " мин.");
        viewHolder.ratio.setText("" + restaurants.get(position).getRatio()+"%");
        return convertView;
    }

    class ViewHolder{
        MyTextViewRoman name, minSum, deliverFree, DeliveryTime, ratio;
        ImageView image;
    }

}
