package com.example.stillhot.app;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.example.stillhot.app.back.Rubrica;
import com.example.stillhot.app.pojo.FourItemsListActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Admin on 11.10.2015.
 */
public class ListAdapter extends ArrayAdapter<Rubrica> {
    private FourItemsListActivity fourItemsListActivity;
    int res;
    float h;
    ArrayList<Rubrica> rubricas;

    public ListAdapter(FourItemsListActivity fourItemsListActivity, Context context, int resource, ArrayList<Rubrica> rubricas, float h) {
        super(context, resource, rubricas);
        this.fourItemsListActivity = fourItemsListActivity;
        this.res = resource;
        this.rubricas = rubricas;
        this.h = h;
    }

    public ListAdapter(FourItemsListActivity fourItemsListActivity, Context context, int resource, float height) {
        super(context, resource);
        this.fourItemsListActivity = fourItemsListActivity;
        this.res = resource;
        this.h = height;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = View.inflate(fourItemsListActivity.getActivity(), res, null);
        ImageView image = (ImageView) convertView.findViewById(R.id.image);
        if (!TextUtils.isEmpty(rubricas.get(position).getIcon())) {
            Picasso.with(getContext()).load(rubricas.get(position).getIcon()).into(image);
        }
        TextView name = (TextView) convertView.findViewById(R.id.name);
        name.setText(rubricas.get(position).getTitle());
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) h / 4);
        image.setLayoutParams(params);
        return convertView;
    }
}
